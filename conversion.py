#l'idée de l'algorithme est que l'on va lire les composantes les unes après les autres, et pour chaque composante générer à chaud ses sommets, en la greffant si besoin est à une composante déjà existante. On va donc garder à tout moment un sommet "libre_x" duquel on n'a pas encore renseigné tous les voisins, pour s'y greffer, et changer le sommet libre_x. Ce faisant on met à jour les tableaux définis ci-dessus qui permettent le maintien des faces / un tracé fidèle
#on appelle donc decoupe_face qui va itérer les composantes, en enracinant chacune en le 1er sommet vu


import networkx as nx
import matplotlib.pyplot as plt

dict_maj = {} #on y stocke l'indice des sommets majuscules déjà croisés

rotation = [] #Afin de savoir si ce sommet a été vu à l'extérieur ou à l'intérieur, et donc dans quel sens tourner : tableau de bouléens

faces = [] #pour parer aux contraintes de la fonction de tracé, on marque pour tout sommet à quelle(s) face(s) il appartient : tableau de tableaux à un ou deux éléments (les indices des faces)

couleurs = [] #on va marquer les sommets morts par des couleurs différentes pour des raisons de lisibilité : tableau de strings correspondants à des couleurs matplotlib

faces_couplees = [] #pour le tracé on se dote d'un gadget qui permet de coupler certaines 

aretes_blanches = [] #pour que l'on  garde en mémoire les arêtes servant pour les gadgets

def affiche_G(G):
    print('[')
    for c in G:
        print('[', end='')
        for k in c:
            print(k, ', ', sep='', end='')
        print('], ', end='')
    print(']')

#on va suivre le parcours le long des arêtes, car on s'est donné pour invariants que l'arête vers le prédecesseur est en indice 0, et celle vers le successeur en indice -1. On fait alors gaffe d'aller voir le successeur ou le prédecesseur, selon le sens de rotation du cycle sur lequel a été construit ce sommet
def prochaine_majuscule(G,v): 
    if rotation[v]:
        u_teste = G[v][-1]
    else:
        u_teste = G[v][0]
    while len(G[u_teste])>2 and u_teste!=v:
        if rotation[u_teste]:
            u_teste = G[u_teste][-1]
        else:
            u_teste = G[u_teste][0]
    return u_teste

#On était couplé à un sommet et le suivant ne l'est plus : on génère donc un sommet intermédiaire pour pouvoir partir de la paroi (d'où le "rhizome")
#On est donc le tableau de listes d'adjacences G, on est placés dans la face `face`, on était couplé au sommet `départ`, et le 1er indice disponible est `départ`
def depart_rhizome(G, face, depart, nb_s):
    global couleurs
    global faces
    if rotation[depart]: #donc si `depart` tourne dans le bon sens
        successeur = G[depart][-1] 
        G.append([successeur, depart]) #on impose une bifurcation : maintenant, le parcours le long du cycle (à l'extérieur de celui qu'on est en train de définir) passera par nb_s+1. On peut se le permettre car on sait que depart n'apparaîtra dans aucune autre composante : on a déjà trouve les deux composantes où il apparaît
        G[successeur][0] = nb_s
        G[depart][-1] = nb_s
    else:
        successeur = G[depart][0]
        G.append([successeur, depart]) #idem
        G[successeur][-1] = nb_s
        G[depart][0] = nb_s
    rotation.append(False) #`detache`, mais on connait sa valeur
    couleurs.append('r') #le sommet engendré sera de degré 3, donc mort
    faces.append([face])

#même chose en symétrique : cette fois on va se greffer à une paroi avant un sommet couplé
#mêmes arguments que ci-dessus, plus "libre_x" pour savoir d'où est-ce qu'on part
def arrivee_rhizome(G, face, cible, nb_s, libre_x):
    global faces
    global couleurs
    G[libre_x].append(nb_s) #et on suit bien la bifurcation
    if rotation[cible]:
        predecesseur = G[cible][0]
        G.append([libre_x, cible, predecesseur]) 
        G[cible][0] = nb_s
        G[predecesseur][-1] = nb_s
    else:
        predecesseur = G[cible][-1]
        G.append([libre_x, cible, predecesseur])  #là aussi, on force la bifurcation pour les prochains parcours
        G[cible][-1] = nb_s
        G[predecesseur][0] = nb_s
    rotation.append(False) #`detache`, mais on connait sa valeur
    couleurs.append('r')
    faces.append([face])

#le "gadget" : on crée un triangle invisible, chacun relié à une face distincte. Cela permet d'éviter certains cas pathologiques de la fonction de tracé où elle pourrait autrement mettre les deux sommets de face sur une même face du dessin par manque de contraintes
def distinction_faces(G, est_couple, v, nb_s):
    global couleurs, faces, rotation, faces_couplees, aretes_blanches
    f1, f2 = faces[est_couple][0], faces[est_couple][1]
    G[est_couple]+=[nb_s, nb_s+1]
    G[v]+=[nb_s, nb_s+1]
    G+=2*[[est_couple, v]]
    rotation+=2*[True] #normalement pas d'importance car on est jamais censés circuler le long de ces sommets invisibles
    couleurs+=2*['w']
    faces+=[[f1], [f2]] #on force donc un tel triangle de chaque côté
    faces_couplees[min(f1, f2)].append(max(f1, f2))
    aretes_blanches+=[(est_couple, nb_s), (est_couple, nb_s+1), (v, nb_s), (v, nb_s+1)]
    return nb_s+2, G


#on regarde une composante enracinée décrite par la string `s` dont le sommet actuellement en déficit de voisins est `libre_init`. Elle se situe dans `face`, le premier indice libre est `nb_precedent`, si `cycle_amont`==True la racine est une majuscule et on devra aller fermer la composante en se rattachant à la racine, et si est_couple != -1 on est enraciné en un sommet majuscule qui existait déjà et se trouve en l'indice `est_couple`
def composante(s, face, nb_precedent, G, libre_init, cycle_amont=False, est_couple = -1):
    if est_couple>=0:
        detache = False #`detache` indique si la composante est détachée (i.e. isolée) lors de sa construction, ou si on contraire elle est attachée à une composante déjà présente
    else:
        detache = True #Tant qu'on pense être détachés de toute autre composante, notre sens de rotation est `True`, sinon (et donc on est en train de lire une majuscule, donc cas traité là-bas) on rattrape nos erreurs et ça devient False
    attache_init = est_couple #afin que l'on ferme correctement le cycle à la fin
    global rotation
    global dict_maj
    global faces
    global couleurs
    n = len(s)
    nb_s=nb_precedent
    print(s, " avec pour indice de départ", libre_init)
    libre_x = libre_init
    cycle_necessaire = cycle_amont #si on rencontre la moindre majuscule, on est obligés de fermer la composante afin de définir une face intérieure, afin que la majuscule ait un sens
    if n==0: #la composante ne comptait donc que sa racine, différente de 0 (sinon cette fonction n'est pas appelée) : on va donc créer une protubérance morte pour compléter la composante. Pour des soucis de ne pas engendrer plus de faces que dans la représentation en string initiale et par incapacité de la fonction utilisée à tracer des multigraphes, on définit abusivement des sommets jaunes, considérés comme morts alors qu'ils ne sont pas de degré 3
        if est_couple==-1:
            if cycle_amont: #on va donc créer un cycle pour que la lettre majuscule soit bien présente dans deux faces à la fois
                G[libre_x]=[nb_s+1, nb_s]
                G.append([libre_x, nb_s+1])
                G.append([nb_s, libre_x])
                rotation+=2*[detache]
                faces+=2*[[face]]
                couleurs+=2*['y']
                nb_s+=2
                return nb_s, G
            else: #on la lie juste à un sommet jaune
                G[libre_x].append(nb_s)
                G.append([libre_x])
                rotation.append(detache)
                faces.append([face])
                couleurs.append('y')
                nb_s+=4
                return nb_s, G
    i =0
    while i<n:
        x = s[i]
        print(x)
        if x=='1':
            if est_couple>=0: #on va alors générer décoller de la paroi et générer libre_x avant par départ_rhizomz
                depart_rhizome(G, face, est_couple, nb_s)
                libre_x = nb_s
                nb_s+=1
                f1, f2 = faces[est_couple][0], faces[est_couple][1]
                if max(f1, f2) not in faces_couplees[min(f1, f2)]: #donc si l'intersection entre ces deux faces n'a pas encore eu de gadget, on va en générer un
                    nb_s, G = distinction_faces(G, est_couple, nb_s-1, nb_s)
                est_couple =-1
            if i==n-1: #libre_x disparaît, car on n'en a plus besoin : c'est ici le dernier sommet de la branche. On va donc vois si on a besoin d'un cycle ou non
                if cycle_amont: #donc si on doit revenir se greffer à la racine
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, libre_init])#on se reconnecte donc au sommet initial
                    G.append([nb_s])
                    rotation+=2*[detache]
                    faces+=2*[[face]]
                    couleurs+=['r', 'b'] #le deuxième sommet crée est celui qui nous intéresse, '1', donc c'est lui que l'on colore en bleu. L'autre, de degré 3, est mort
                    successeur_init = G[libre_init][0]
                    G[libre_init][0]=nb_s #on corrige alors les adjacences
                    G[libre_init].append(successeur_init) #gymnastique pour que le sens de rotation de 'A' soit celui voulu
                    nb_s+=2
                elif cycle_necessaire or ((not detache) and attache_init ==-1):
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, nb_s+2]) #nb_s+1 sera donc inséré au début, nb_s+2 sera le sommet 1
                    G.append([nb_s,libre_init, nb_precedent])
                    G.append([nb_s])
                    rotation+=3*[detache]
                    faces+=3*[[face]]
                    couleurs+=['r', 'r', 'b']#et on colore en concordance
                    #puis on va aller changer les voisinages pour que cela forme un cycle
                    G[libre_init][-1]=nb_s+1 #on insére donc libre_init avant le 1er sommet de cette sous-composante
                    G[nb_precedent][0]=nb_s+1 #on a fait gaffe à toujours mettre l'arête vers libre_x en indice 0
                    nb_s+=3
                elif attache_init>=0: #on va alors clore la composante en se greffant juste avant le 1er sommet (celui où on l'avait enraciné)
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+2]) #nb_s+2 sera le sommet 1
                    couleurs.append('r')
                    nb_s+=1
                    arrivee_rhizome(G, face, attache_init, nb_s, nb_s-1)
                    G.append([nb_s])
                    rotation+=2*[False] #`detache`, mais sa valeur est connue
                    faces+=2*[[face]]
                    couleurs.append('b')
                    nb_s+=2
                else: #on peut alors juste mettre un sommet à un seul voisin au bout de la composante : pas besoin de cycle
                    G[libre_x].append(nb_s)
                    G.append([libre_x])
                    rotation+=[detache] #normalement pas nécessaire de garder le sens de rotation pour cette composante alors, mais il faut que `rotation` soit de la bonne taille
                    faces.append([face])
                    couleurs.append('b')
                    nb_s+=1
            else: #on a besoin d'engendrer un nouveau libre_x : on va donc créer un sommet de degré 3 reliant l'ancien libre_x et notre sommet de degré 1, puis il deviendra lui-même libre_x
                G[libre_x].append(nb_s)
                G.append([libre_x, nb_s+1])
                G.append([nb_s])
                rotation+=2*[detache]
                faces+=2*[[face]]
                couleurs.append('r', 'b')
                libre_x = nb_s
                nb_s+=2;
            i+=1
        elif x=='2': #on rajoute à chaque fois un arc mort derrière pour éviter qu'il ne se transforme en 'a' ou 'A', ce qui fait qu'on a généralement besoin de trois sommets. Le deuxième du groupe de trois est celui qui nous intéresse
            if est_couple>=0: #on va alors générer libre_x avant
                depart_rhizome(G, face, est_couple, nb_s)
                nb_s+=1
                f1, f2 = faces[est_couple][0], faces[est_couple][1]
                if max(f1, f2) not in faces_couplees[min(f1, f2)]:
                    nb_s, G = distinction_faces(G, est_couple, nb_s-1, nb_s)
                est_couple =-1
            if i == n-1:#idem, même disjonction de cas
                if cycle_amont:
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, nb_s+2])
                    G.append([nb_s, nb_s+2])
                    G.append([nb_s, nb_s+1, libre_init])
                    rotation+=3*[detache]
                    faces+=3*[[face]]
                    couleurs+=['r', 'b', 'r']
                    successeur_init = G[libre_init][0]
                    G[libre_init][0]=nb_s+2
                    G[libre_init].append(successeur_init)
                    nb_s+=3
                elif cycle_necessaire or ((not detache) and attache_init ==-1):
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, nb_s+2])
                    G.append([nb_s, nb_s+2])
                    G.append([nb_s, nb_s+1, nb_s+3])
                    G[libre_init][-1]=nb_s+3 
                    G[nb_precedent][0]=nb_s+3
                    G.append([nb_s+2, libre_init, nb_precedent])
                    rotation+=4*[detache]
                    faces+=4*[[face]]
                    couleurs+=['r', 'b', 'r', 'r']
                    nb_s+=4
                elif attache_init>=0: #on va alors clore la composante juste avant le 1er sommet (celui où on l'avait enraciné)
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, nb_s+2])
                    G.append([nb_s, nb_s+2])
                    G.append([nb_s, nb_s+1])
                    nb_s+=3
                    rotation+=3*[False] #valeur connue
                    faces+=3*[[face]]
                    couleurs+=['r', 'b', 'r']
                    arrivee_rhizome(G, face, attache_init, nb_s, nb_s-1)
                    nb_s+=1
                else:
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, nb_s+2])
                    G.append({nb_s, nb_s+2, nb_s+3})
                    G.append([nb_s, nb_s+1, nb_s+3])
                    G.append([nb_s+2, nb_s+1])
                    rotation+=4*[detache] #idem, censément inutile
                    faces+=4*[[face]]
                    couleurs+=['r', 'r', 'r', 'b']
                    nb_s +=4
            else:
                G[libre_x].append(nb_s)
                G.append([libre_x, nb_s+1, nb_s+2])
                G.append([nb_s, nb_s+2])
                G.append([nb_s, nb_s+1])
                rotation+=3*[detache]
                faces+=3*[[face]]
                couleurs+=['r', 'b', 'r']
                libre_x = nb_s+2
                nb_s+=3
            i+=1
        elif ord(x)>=97 and ord(x)<123: #on va créer un appendice comme pour '1', puis faire un autre appel à `composante` entre les deux occurences de la minuscule en question. On crée dans l'idée une sous-composante, que l'on va enraciner en cette minuscule. Les soucis de sens de rotation conservé entre celle-ci et son fils ne sont pas pertinents, car quitte à considérer la première composante tracée comme définissant l'extérieur, toutes les autres définissent une face à l'intérieur : un sommet minuscule ne peut alors pas relier deux majuscules en lien avec faces différentes, à moins qu'une des deux soit "intérieure" à la boucle ici définie : on peut alors bien se permettre de prendre un sens de rotation "détaché" pour la définir
            if est_couple>=0: #on va alors générer libre_x avant
                depart_rhizome(G, face, est_couple, nb_s)
                libre_x = nb_s
                nb_s+=1
                f1, f2 = faces[est_couple][0], faces[est_couple][1]
                if max(f1, f2) not in faces_couplees[min(f1, f2)]:
                    nb_s, G = distinction_faces(G, est_couple, nb_s-1, nb_s)
                est_couple =-1
            x2 = s.find(x,i+1)
            s2 = s[i+1: x2]
            if x2 == n-1:
                if cycle_amont:
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, libre_init]) #nb_s+1 sera donc inséré au début, nb_s+2 sera le sommet minuscule
                    G.append([nb_s])
                    successeur_init = G[libre_init][0]
                    G[libre_init][0]=nb_s
                    G[libre_init].append(successeur_init)
                    rotation+=2*[detache]
                    faces+=2*[[face]]
                    couleurs+=['r', 'b']
                    nb_s+=2
                elif cycle_necessaire or ((not detache) and attache_init ==-1):
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+1, nb_s+2]) #nb_s+1 sera donc inséré au début, nb_s+2 sera le sommet minuscule
                    G.append([nb_s,libre_init, nb_precedent])
                    G[libre_init][-1]=nb_s+1
                    G[nb_precedent][0]=nb_s+1 
                    G.append([nb_s]) #finalement on rajoute notre sommet minuscule
                    rotation+=3*[detache]
                    faces+=3*[[face]]
                    couleurs+=['r', 'r', 'b']
                    nb_s+=3
                elif attache_init>=0:
                    G[libre_x].append(nb_s)
                    G.append([libre_x, nb_s+2]) #nb_s+2 sera le sommet minuscule
                    nb_s+=1
                    couleurs.append('r')
                    arrivee_rhizome(G, face, attache_init, nb_s, nb_s-1)
                    G.append([nb_s-1]) #on rajoute donc finalement la minuscule, reliée à notre sommet de degré 3 (le nb_s initial)
                    rotation+=2*[False] #là encore on connait sa valeur
                    faces+=2*[[face]]
                    couleurs.append('b')
                    nb_s+=2
                else:
                    G[libre_x].append(nb_s)
                    G.append([libre_x])
                    rotation.append(detache)
                    faces.append([face])
                    couleurs.append('b')
                    nb_s +=1
            else:
                G[libre_x].append(nb_s)
                G.append([libre_x, nb_s+1])
                G.append([nb_s])
                rotation+=2*[detache]
                faces+=2*[[face]]
                couleurs+=['r', 'b']
                libre_x = nb_s #et c'est l'arete libre qu'on gardera lorsqu'on reprendra notre parcours
                nb_s+=2
            nb_s, G = composante(s2, face, nb_s, G, nb_s-1)
            i = x2+1
        elif ord(x)>=65 and ord(x)<91:
            if x in dict_maj.keys():#donc si le sommet a déjà été construit
                faces[dict_maj[x]].append(face)
                if detache: #on note qu'on est alors rattachés à une composante existante, et on va donc changer le sens de rotation de tous les sommets crées depuis le début de cette composante pour signifier qu'on est à l'intérieur (en donnant pour extérieur la première face tracée du bloc)
                    for j in range(nb_precedent,nb_s):
                        rotation[j]=False
                    rotation[libre_init]=False
                    detache=False
                if est_couple>=0: #donc le précédent sommet vu était un sommet frontière, et on regarde si on reste sur la frontière
                    if dict_maj[x] != prochaine_majuscule(G, est_couple): #donc si le nouveau sommet n'est pas la prochaine majuscule à apparaître à l'intérieur de l'ancienne composante, on va créer un rhizome partant de celui-ci au suivant
                        depart_rhizome(G, face, est_couple, nb_s)
                        nb_s+=1
                        arrivee_rhizome(G, face, dict_maj[x], nb_s, nb_s-1)
                        nb_s+=1
                    #else rien à faire : les deux sommets sont déjà voisins par construction
                    est_couple = dict_maj[x]
                else: #on va donc juste s'insérer en frontière
                    arrivee_rhizome(G, face,dict_maj[x], nb_s, libre_x)
                    est_couple = dict_maj[x]
                    nb_s+=1
                if i==n-1: #on va alors repartir de la paroi pour fermer la boucle
                    depart_rhizome(G, face, est_couple, nb_s)
                    libre_x = nb_s
                    nb_s+=1
                    f1, f2 = faces[est_couple][0], faces[est_couple][1]
                    if max(f1, f2) not in faces_couplees[min(f1, f2)]: #là encore, même vérification
                        nb_s, G = distinction_faces(G, est_couple, nb_s-1, nb_s)
                    if cycle_amont:
                        G[libre_x].append(libre_init)
                        successeur_init = G[libre_init][0]
                        G[libre_init][0]=libre_x
                        G[libre_init].append(successeur_init)
                    elif attache_init>=0:
                        G[libre_x].append(attache_init)
                        arrivee_rhizome(G, face, attache_init, nb_s, libre_x)
                        nb_s+=1
                    else:
                        G[libre_x].append(nb_s)
                        G.append([libre_x,libre_init, nb_precedent])
                        #puis on va aller changer les voisinages pour que cela forme un cycle
                        G[libre_init][-1]=nb_s #on insére donc libre_init avant le 1er sommet de cette sous-composante
                        G[nb_precedent][0]=nb_s #on a fait gaffe à toujours mettre l'arête vers libre_x en indice 0
                        rotation.append(detache)
                        faces.append([face])
                        couleurs+=['r']
                        nb_s+=1
            else:
                if est_couple>=0: #on va alors générer libre_x avant
                    depart_rhizome(G, face, est_couple, nb_s)
                    libre_x = nb_s
                    print("on est passés par ici, libre_x =", libre_x)
                    nb_s+=1
                    f1, f2 = faces[est_couple][0], faces[est_couple][1]
                    if max(f1, f2) not in faces_couplees[min(f1, f2)]:
                        nb_s, G = distinction_faces(G, est_couple, nb_s-1, nb_s)
                        print("après distinction des faces, nb_s =", nb_s)
                    est_couple =-1
                dict_maj[x]=nb_s
                if detache:
                    cycle_necessaire=True #si on rencontre une lettre majuscule, on est obligés de former un cycle pour définir une autre face
                #alors que si on est un rhizome, on sait qu'on va de toute façon fermer la composante
                if i==n-1:#toujours la même disjonction de cas
                    if cycle_amont:
                        if i==0:
                            G[libre_x].append(nb_s)
                            G.append([libre_x])
                            rotation.append(detache)
                            faces.append([face])
                            couleurs.append('y')
                            libre_x=nb_s
                            nb_s+=1                            
                        G[libre_x].append(nb_s)
                        G.append([libre_x, libre_init])
                        successeur_init = G[libre_init][0]
                        G[libre_init][0]=nb_s
                        G[libre_init].append(successeur_init)
                        rotation+=[detache]
                        faces.append([face])
                        couleurs.append('b')
                        nb_s+=1
                    elif attache_init>=0:
                        G[libre_x].append(nb_s)
                        G.append([libre_x])
                        nb_s+=1
                        couleurs.append('b')
                        arrivee_rhizome(G, face, attache_init, nb_s, nb_s-1)
                        rotation.append(detache)
                        faces.append([face])
                        nb_s+=1
                    else:
                        G[libre_x].append(nb_s)
                        G.append([libre_x, nb_s+1])
                        G.append([nb_s,libre_init, nb_precedent])
                        G[libre_init][-1]=nb_s+1 
                        G[nb_precedent][0]=nb_s+1 
                        rotation+=2*[detache]
                        faces+=2*[[face]]
                        couleurs+=['b', 'r']
                        nb_s+=2
                else:
                    print('nb_s =', nb_s)
                    G[libre_x].append(nb_s)
                    G.append([libre_x])
                    libre_x = nb_s
                    rotation+=[detache]
                    faces.append([face])
                    couleurs.append('b')
                    nb_s+=1
            i+=1
        affiche_G(G)
    return nb_s, G

#On prend comme argument la string `s` décrivant une "land", puis on va itérer, par face, les composantes qui en font partie, en construisant le graphe au fur et à mesure
def decoupe_face(s): #s doit donc déjà être une "land"
    #On initialise tous les tableaux globaux au tableau vide
    global rotation
    rotation = []
    global dict_maj
    dict_maj = {}
    G=[]
    global faces
    faces=[]
    global couleurs
    couleurs = []
    global faces_couplees
    faces_couplees = []
    global aretes_blanches
    aretes_blanches = []
    nb_faces=0
    s_faces = s.split("|") #on génère donc la liste des faces
    n=0
    for F in s_faces:
        faces_couplees.append([])
        liste = F.split(".") #puis la liste des composantes de cette face
        for code in liste:
            code = code.strip(' ') #on se débarrasse des éventuels ' '
            if len(code)>=1: #car si l'utilisateur nous met un '.' en fin de string, split() va générer une string vide : on la néglige alors
                x=code[0] #on regarde donc le premier sommet de la composante, en lequel on va s'enraciner
                if x=='0':
                    n+=1
                    G+=[[]] #on rajoute donc juste un sommet vide : si la code commence par 0, ce n'est que 0
                    rotation.append(True)
                    faces.append([nb_faces])
                    couleurs.append('b')
                elif x=='1': #on fixe le 1er sommet comme libre_x, puis on fait le contour
                    rotation.append(True) #on met à jour les tableaux avec l'ajout de ce nouveau sommet
                    faces.append([nb_faces])
                    couleurs.append('b')
                    n, G = composante(code[1:], nb_faces, n+1, G+[[]], n)
                elif ord(x)>=65 and ord(x)<91:
                    if x in dict_maj.keys():
                        faces[dict_maj[x]].append(nb_faces) #on oublie pas de préciser, même si les autres tableaux on déjà été remplis, qu'on sait maintenant à quelle autre face il appartient
                        n, G = composante(code[1:], nb_faces, n, G, dict_maj[x], False, dict_maj[x])
                    else:
                        dict_maj[x]=n 
                        rotation.append(True)
                        faces.append([nb_faces])
                        couleurs.append('b')
                        n, G = composante(code[1:], nb_faces, n+1, G+[[]], n, True, -1) #on veut qu'il y ait un cycle amont, i.e. qu'on se reconnecte au libre_init
                elif ord(x)>=97 and ord(x)<123:#on traite alors un côté, puis l'autre, tel l'appel récursif généralement fait dans `composante`
                    rotation.append(True)
                    faces.append([nb_faces])
                    couleurs.append('b')
                    x2 = code.find(x,1)
                    c2 = code[1: x2]
                    nb, composante1 = composante(c2, nb_faces, n+1, G+[[]], n)
                    c3 = code[x2+1:]
                    n, G = composante(c3, nb_faces, composante1, n) #et n sert donc à nouveau de `libre_x` : on a donc bien généré notre composante de part et d'autre des deux 'a' (c'est nécessairement un 'a' par ordre lexicographique en fait)
                else: #donc ça correspond à un 2
                    rotation+=4*[True]
                    faces+=4*[[nb_faces]]
                    couleurs+=['b', 'r', 'r', 'r']
                    n, G = composante(code[1:], nb_faces, n+4, G+[[n+1, n+2], [n, n+2, n+3], [n, n+1, n+3], [n+1, n+2]], n+3)
        nb_faces+=1
        affiche_G(G)
    return G




def trace(adjacences, faces, couleurs):
    global aretes_blanches #on va donc regarder quelles sont les arêtes qui ont été crées par les gadgets, et donc que l'on devra colorier en blanc
    G = nx.Graph()
    n = len(adjacences)
    G.add_nodes_from([k for k in range(n)])
    couleur_e = []
    nb_f = 0
    for adj in faces:
        for x in adj:
            if x>=nb_f: 
                nb_f = x
    G.add_nodes_from([k for k in range(n, n+nb_f)])
    for i in range(n):
        for j in adjacences[i]:
            if j>=i: #on ne veut pas mettre les arêtes en double, car il ne sait pas tracer les multigraphes
                G.add_edge(i,j)
                if (i,j) in aretes_blanches:
                    couleur_e.append('w')
                else:
                    couleur_e.append('r')
        for f in faces[i]: #on rajoute les arêtes vers les sommets de face
            G.add_edge(i, n+f)
            couleur_e.append('w')
    couleur_node = couleurs+(nb_f+1)*['w'] #les derniers sommets, ceux de face, sont invisibles
    nx.draw_planar(G, edge_color = couleur_e, node_color = couleur_node, node_size = 100)
    plt.show()

G=[]
G = decoupe_face("AB2C|DEA")
trace(G, faces, couleurs)
